Bootstrap 3 Basis Extension
===========================
---
## Extension-Key: ot_bootstrap3

A TYPO3 website in one extension with Bootstrap 3.3.6

All content elements will have an additional tab for the Bootstrap Grid CSS classes.
Additional Backend-Layouts are added with PageTSconfig.

How to use
==========

1. On your root page insert a new TypoScript Template

2. Insert the next line in the **setup** field:

   ```
   <INCLUDE_TYPOSCRIPT: source="FILE:EXT:ot_bootstrap3/Configuration/TypoScript/setup.txt">
   ```

3. Insert the next line in the **constants** field:

   ```
   <INCLUDE_TYPOSCRIPT: source="FILE:EXT:ot_bootstrap3/Configuration/TypoScript/constants.txt">
   ```
   
4. Insert the **PageTSConfig** from this extension in the page properties of the root page.
  Tab: Resources
  Field: Include Page TSConfig (from extensions):

5. Clear the page cache




# CSS


## Abstände bei Überschriften in Abhängigkeit der Fensterbreite (xs/sm/md/lg)

.mt-hx-xs-0
	Setzt Abstand oben von untergeordneten Überschriften bei Fensterbreite Extra-Small auf 0px
	
.mt-hx-md-30  
	Setzt Abstand oben von untergeordneten Überschriften bei Fensterbreite Extra-Small auf 30px


## Abstände bei Containern (z.B. Content Element-> Tab Bootstrap -> CSS (outer div)

.mt-none
	Setzt Abstand oben auf 0px
	
.mt-medium 
	Setzt Abstand oben auf 15px
	
.mt-large
	Setzt Abstand oben auf 30px
	


## Abstände bei Containern in Abhängigkeit der Fensterbreite (z.B. Content Element -> Tab Bootstrap -> CSS (outer div)

.mt-md-medium
	Abstand oben bei Fensterbreite medium: 15px
	
.mt-sm-large
	Abstand oben bei Fensterbreite Small: 30px
